<?php
function getMeta($title)
{
    $domaine = "";
    $extension = ".html";
    $url = $domaine . $title . $extension;
    $value = getXpath($url); 
    if (empty($value)) return null;
    $xpath =  $value->xpath;
    $meta = $xpath->query('//meta[@name="description"]')->item(0); 
    if (isset($meta) && $meta) {
        $meta_label = $meta->getAttribute('content'); 
        if(isset($meta_label) && $meta_label) {
            return $meta_label;
        } else {
            return "";
        }
    } else {
        echo "Aucune balise meta avec name='description' trouvée.<br>";
        return "";
    }
}
