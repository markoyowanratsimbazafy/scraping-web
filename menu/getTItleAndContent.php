<?php

require './connexion/connexion_ftp.php';
require 'getContent.php';
require 'getMeta.php';
require 'extractTitle.php';
require 'insertPage.php';

function getTitleAndContent($bd, $domaine_url , $remplace_domaine)
{ 
    $directory = '';
   // $local_dir = 'chemin_local/';

    $sftp = connectToSFTP($directory);

    $files = $sftp->nlist('.');

    // Fonction pour extraire le titre avant ".tpl.php"

    // Nouveau tableau pour stocker les objets JSON
    $jsonObjects = array();

    // Parcourir le tableau initial
    foreach ($files as $file) {
        // Exclure les entrées "." et ".." ainsi que les entrées contenant "_MODELE"
        if ($file !== "." && $file !== ".." && strpos($file, '_MODELE') === false) {
            // Extraire le titre
            $titre = extractPostName($file); 
            // Créer un objet JSON
            $jsonObject = array(
                "post_title" => extractPostTile($titre),
                "post_content" => getContent($titre , $domaine_url , $remplace_domaine),
                "post_status" => 'publish',
                "post_name" => $titre,
                "post_author"   => 1,
                "post_type"     => 'page',
                "meta_description"     => getMeta($titre)
            );
            $response =  insertTablePost($jsonObject,$bd); 
            // Ajouter l'objet au tableau
            $jsonObsjects[] = $response;
        }
    }

    var_dump($jsonObjects);

    return $jsonObjects;
}
