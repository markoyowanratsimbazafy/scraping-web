<?php
// URL de la page à scraper
$url = '';

// Initialisation de cURL
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// Exécution de la requête cURL
$html = curl_exec($ch);

// Fermeture de la session cURL
curl_close($ch);

// Création d'un objet DOMDocument
$dom = new DOMDocument();

// Suppression des avertissements relatifs à la mauvaise formation du HTML
libxml_use_internal_errors(true);

// Chargement du HTML dans l'objet DOMDocument
$dom->loadHTML($html);

// Restauration des avertissements
libxml_use_internal_errors(false);

// Création d'un objet DomXPath pour effectuer des requêtes XPath sur le document HTML
$xpath = new DomXPath($dom); 

// Requête XPath pour obtenir les éléments à partir de la balise avec l'ID "header"
$headerContent = $xpath->query('//div[@id="header"]')->item(0);
var_dump($headerContent);
// Vérification si la balise avec l'ID "header" existe
if ($headerContent) {
    // Requête XPath pour obtenir les menus, titres et sous-menus
    $menus = $xpath->query('.//ul[contains(@class, "menu")] | .//a[contains(@class, "sf-with-ul")] | .//ul[contains(@class, "sub-menu")]', $headerContent);
    var_dump($menus);
    // Parcours des résultats et affichage du contenu
    foreach ($menus as $menu) {
        echo $dom->saveHTML($menu) . PHP_EOL;
    }
} else {
    echo "La balise avec l'ID 'header' n'a pas été trouvée.";
}

?>