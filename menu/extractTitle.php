<?php
function extractPostName($filename)
{
    // Trouver la position de ".tpl.php"
    $pos = strpos($filename, '.tpl.php');

    // Si ".tpl.php" est trouvé, extraire le titre
    if ($pos !== false) {
        return substr($filename, 0, $pos);
    }

    // Si ".tpl.php" n'est pas trouvé, retourner le nom de fichier tel quel
    return $filename;
}


function extractPostTile($filename)
{
    // Séparer la chaîne en mots
    $words = explode('-', $filename);

    // Mettre en majuscule la première lettre de la première phrase
    $words[0] = ucfirst(strtolower($words[0]));

    // Reconstruire la chaîne
    $result = implode(' ', $words);

    return $result;
}
