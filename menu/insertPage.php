<?php
require './connexion/connexion_BD.php';
require './share/insert_standard.php';
require './share/get_permalink.php';
try {
    function insertTablePost($data,$bd)
    {
        $bdd = connexionBD($bd);
        $post_title = mysqli_real_escape_string($bdd ,$data["post_title"]);
        $post_content = mysqli_real_escape_string($bdd,$data["post_content"]);
        $post_status = $data["post_status"];
        $post_name = mysqli_real_escape_string($bdd,$data["post_name"]);
        $post_author = $data["post_author"];
        $post_type = $data["post_type"];
        $meta_description = mysqli_real_escape_string($bdd , $data["meta_description"]);
        $query = "INSERT INTO wp_posts (post_title ,post_content,post_status,post_name,post_author,post_type ) VALUES ( '$post_title','$post_content','$post_status','$post_name',$post_author,'$post_type' )";
        // $bdd = connexionBD("canibest");
        $result =  $bdd->query($query);
        if ($result) {
            $page_id = $bdd->insert_id;
            // Insérer ou mettre à jour le SEO Title dans la table wp_postmeta
            insert_Wp_postmeta_title($bdd, $page_id, $post_title);
            // Insérer ou mettre à jour le SEO Description dans la table wp_postmet
            insert_Wp_postmeta_description($bdd, $page_id, $meta_description);
            // Insérer ou mettre à jour le SEO Title dans la table wp_yoast_indexable
            insert_Wp_yoast_indexable_page($bdd, $page_id, $meta_description, $post_title, get_permalink("page", $post_name));
            return "Réussi : $post_title";
        } else {
            return "Erreur : $post_title";
        }
    } 

} catch (PDOException $e) {
    echo "Erreur de connexion à la base de données : " . $e->getMessage();
}
