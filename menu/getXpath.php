<?php
function getXpath($url_data)
{
    // URL de la page à scraper
    $url = $url_data;

    // Initialisation de cURL
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // Exécution de la requête cURL
    $html = curl_exec($ch);

    // Fermeture de la session cURL
    curl_close($ch);

    // Création d'un objet DOMDocument
    $dom = new DOMDocument();

    // Suppression des avertissements relatifs à la mauvaise formation du HTML
    libxml_use_internal_errors(true);

    // Chargement du HTML dans l'objet DOMDocument
    if (empty($html)) return null;
    $dom->loadHTML($html);

    // Restauration des avertissements
    libxml_use_internal_errors(false);

    // Création d'un objet DomXPath pour effectuer des requêtes XPath sur le document HTML
    $xpath = new DomXPath($dom);
    return new Resultat($xpath, $dom);
}


class Resultat
{
    public $xpath;
    public $dom;

    public function __construct($xpath, $dom)
    {
        $this->xpath = $xpath;
        $this->dom = $dom;
    }
}
