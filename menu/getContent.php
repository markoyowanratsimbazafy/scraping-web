<?php
require 'getXpath.php';

function getContent($title , $domaine_url , $remplace_domaine)
{
    $domaine = $domaine_url;
    $extension = ".html";
    $url = $domaine . $title . $extension;
    $value = getXpath($url);
    if (empty($value)) return null;
    $xpath =  $value->xpath;
    $dom = $value->dom;
    // Requête XPath pour obtenir la section avec l'ID "primary-left"
    $primaryLeftSection = $xpath->query('//div[@id="primary-left"]')->item(0);

    // Vérification si la balise avec l'ID "primary-left" existe
    if (isset($primaryLeftSection) && $primaryLeftSection) {
     // Obtenez le contenu brut de la section "primary-left"
     $content = $dom->saveHTML($primaryLeftSection);

     // Remplacez les liens dans le contenu
     $content = str_replace($domaine, $remplace_domaine, $content);
 
     // Affichage du contenu modifié
     return $content;
    } else {
        return "La balise avec l'ID 'primary-left' n'a pas été trouvée.";
    }
}
