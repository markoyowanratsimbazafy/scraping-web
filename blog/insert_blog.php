<?php
require './connexion/connexion_BD.php';
require './share/get_permalink.php';
require './share/insert_standard.php';
require 'insertImageVignettes.php';
try {
    function insertTableBlog($data, $dataImage, $lien_image)
    {
        $bdd = connexionBD("");
        $id_article = mysqli_real_escape_string($bdd, $data["id_article"]);
        $post_title = mysqli_real_escape_string($bdd, $data["title"]);
        $post_content =  mysqli_real_escape_string($bdd, $data["content"]);
        $post_status = "publish";
        $post_name = mysqli_real_escape_string($bdd, $data["slug"]);;
        $post_author = 1;
        $post_type = 'post';
        $description = mysqli_real_escape_string($bdd, $data['description']);
        $query = "INSERT INTO wp_posts (post_title ,post_content,post_status,post_name,post_author,post_type ) VALUES ( '$post_title','$post_content','$post_status','$post_name',$post_author,'$post_type' )";
        // $bdd = connexionBD("canibest");

        $result =  $bdd->query($query);
        if ($result) {
            // Si l'insertion s'est bien déroulée, vous pouvez obtenir l'ID avec insert_id
            $article_id = $bdd->insert_id;
            // Insertion mise en avant image 
            $new_path_image = $id_article . '-' . $post_name;
            $responseImage =  checkImageVignettes($bdd, $new_path_image, $article_id, $dataImage, $lien_image);
            //Update
            $new_post_name = $article_id . '-' . $post_name;
            updateNewName($new_post_name, $article_id);

            $result_cat = getIdCategory();
            if ((isset($article_id) && $article_id) && (isset($result_cat) && $result_cat)) {
                insertionTermRelationships($article_id, $result_cat);
            }
            // echo "L'ID de l'article inséré est : " . $article_id;


            if ($post_title && $description) {
                // Insérer ou mettre à jour le SEO Title dans la table wp_postmeta
                insert_Wp_postmeta_title($bdd, $article_id, $post_title);
                // Insérer ou mettre à jour le SEO Description dans la table wp_postmet
                insert_Wp_postmeta_description($bdd, $article_id, $description);
                // Insérer ou mettre à jour le SEO Title dans la table wp_yoast_indexable
                insert_Wp_yoast_indexable_blog($bdd, $article_id, $description, $post_title, get_permalink("blog", $post_name), $responseImage);
                insert_Wp_postmeta__thumbnail_id($bdd, $article_id,$responseImage);
            } else {
                echo "Une erreur est survenue lors de l'insertion du meta";
            }
        } else {
            // Gestion des erreurs
            echo "Erreur lors de l'insertion : " . $bdd->error;
        }

        if ($result)  return "Réussi : $post_title";
        else return "Erreur : $post_title";
    }


    function getIdCategory()
    {
        $query = "SELECT term_id FROM wp_terms WHERE slug = 'blog'";
        // $bdd = connexionBD("canibest");
        $bdd = connexionBD("zoopro_preprod");
        $result =  $bdd->query($query);

        if (isset($result) && isset($result->num_rows)  && $result->num_rows > 0) {
            $row_category = $result->fetch_assoc();
            $category_id = $row_category['term_id'];
            return $category_id;
        } else {
            return null;
        }
    }

    function insertionTermRelationships($id_article, $id_category)
    {
        var_dump("id_article", $id_article);
        var_dump("id_category", $id_category);
        $query = "INSERT INTO wp_term_relationships (object_id ,term_taxonomy_id ) VALUES ($id_article , $id_category)";
        // $bdd = connexionBD("canibest");
        $bdd = connexionBD("zoopro_preprod");
        $result =  $bdd->query($query);
    }

    function updateNewName($new_post_name, $article_id)
    {
        $update_postname = "UPDATE wp_posts SET post_name = '$new_post_name' WHERE id = '$article_id'";
        $bdd = connexionBD("zoopro_preprod");
        $bdd->query($update_postname);
    }
} catch (PDOException $e) {
    echo "Erreur de connexion à la base de données : " . $e->getMessage();
}
