<?php
 
require './connexion/connexion_ftp.php';
require 'insert_blog.php';
require 'getImageVignettes.php';
try {
    function getBlog($lien_image)
    {

        $valueImageVignette = getImageVignettes(); 
        $sql = "SELECT * FROM blog";
        $bdd = connexionBD("zoopro");
        $resultat = $bdd->query($sql);

        $data_blog = array();

        if (isset($resultat) && isset($resultat->num_rows) && $resultat->num_rows > 0) {
            // Boucle à travers les résultats
            foreach ($resultat as $file) {
                // Récupération des données dans la table zoopro et de l'insérer dans la table canibest qui est de côté wordpress après
                $data_blog  = array(
                    'id_article'   => $file['id_article'],
                    'title'   => $file['titre'],
                    'content' => $file['contenu'],
                    'date'    => $file['date'],
                    'slug'    => $file['slug'],
                    'description' => $file['description']
                );
                $response =  insertTableBlog($data_blog , $valueImageVignette,$lien_image);
                $jsonObsjects[] = $response;
                return;
            }
        } else {
            echo "Aucun résultat trouvé";
        }
    }
} catch (PDOException $e) {
    echo "Erreur de connexion à la base de données : " . $e->getMessage();
}
