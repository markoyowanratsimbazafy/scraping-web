<?php
//require '../connexion/connexion_ftp.php';

function getImageVignettes()
{
    $directory = '/www/zoopro/dyn/blog/images/vignettes';
    // $local_dir = 'chemin_local/';

    $sftp = connectToSFTP($directory);

    $files = $sftp->nlist('.');
    $jsonObjects = array();
    foreach ($files as $file) {
        $pathinfo = pathinfo($file);
        $jsonObjects = array(
            'title'   => $file,
            'pathInfo' =>  $pathinfo['filename'] , 
            'extension' =>  $pathinfo['extension']
        );

        $jsonObsjects[] = $jsonObjects;
    }

    return $jsonObsjects;
}
