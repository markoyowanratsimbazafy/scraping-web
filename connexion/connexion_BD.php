<?php

function connexionBD($table)
{
    $host = "localhost";
    $nom_utilisateur = "root";
    $mot_de_passe = "";
    $nom_base_de_donnees = $table;

    try {
        // Crée une instance de la classe PDO pour établir une connexion 
        $bdd = new mysqli($host, $nom_utilisateur, $mot_de_passe, $nom_base_de_donnees);
        $bdd->set_charset("utf8mb4"); 
        return  $bdd;
    } catch (PDOException $e) {
        echo "Erreur de connexion à la base de données : " . $e->getMessage();
    }

    if ($bdd->connect_errno) {
        echo "Failed to connect to MySQL: " . $bdd->connect_error;
        exit();
    }
}
