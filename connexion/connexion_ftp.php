<?php
require 'vendor/autoload.php';

use phpseclib3\Net\SFTP;

function connectToSFTP($directory)
{
    $host = "";
    $user = "";
    $password = '';

    $sftp = new SFTP($host);

    if (!$sftp->login($user, $password)) {
        die("Échec de l'authentification SFTP.\n");
    }

    $sftp->chdir($directory);

    return $sftp;
}
