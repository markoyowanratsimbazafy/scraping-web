<?php

function insert_Wp_postmeta_title($bdd, $id, $post_title)
{
    $query = "INSERT INTO wp_postmeta (post_id, meta_key, meta_value)
    VALUES ('$id', '_yoast_wpseo_title', '$post_title')
    ON DUPLICATE KEY UPDATE meta_value = '$post_title'";
    $result =  $bdd->query($query);
    if (isset($result) && $result) {
        echo "Ajout meta title réussi  <br>";
    } else {
        echo "Erreur ajout meta title";
    }
}

function insert_Wp_postmeta_description($bdd, $id, $description)
{
    // Insérer ou mettre à jour le SEO Description dans la table wp_postmeta
    $query = "INSERT INTO wp_postmeta (post_id, meta_key, meta_value)
    VALUES ('$id', '_yoast_wpseo_metadesc', '$description')
    ON DUPLICATE KEY UPDATE meta_value = '$description'";
    $result =  $bdd->query($query);
    if (isset($result) && $result) {
        echo "Ajout meta description réussi <br>";
    } else {
        echo "Erreur ajout meta description <br>";
    }
}

function insert_Wp_yoast_indexable_blog($bdd, $id, $description, $post_title, $get_permalink, $responseImage)
{
    $id_image = $responseImage['id_image'];
    $url_image = $responseImage['url_image'];
    // Insérer ou mettre à jour le SEO Title dans la table wp_yoast_indexable
    $query = "INSERT INTO wp_yoast_indexable (object_id, object_type, title, description, permalink , twitter_image , twitter_image_id, twitter_image_source,open_graph_image , open_graph_image_id, open_graph_image_source )
            VALUES ('$id', 'post', '$post_title', '$description', '" . $get_permalink . "', '$url_image' ,$id_image , 'featured-image' , '$url_image' ,$id_image  , 'featured-image'  )";
    // ON DUPLICATE KEY UPDATE title = '$post_title', description = '$description', permalink = '" . $get_permalink . "'";
    $result =  $bdd->query($query);
    if (isset($result) && $result) {
        echo "Ajout indexation  meta title et description réussi <br>";
    } else {
        echo "Erreur ajout indexation meta title et description <br>";
    }
}

function insert_Wp_yoast_indexable_page($bdd, $id, $description, $post_title, $get_permalink)
{
    // Insérer ou mettre à jour le SEO Title dans la table wp_yoast_indexable
    $query = "INSERT INTO wp_yoast_indexable (object_id, object_type, title, description, permalink )
            VALUES ('$id', 'post', '$post_title', '$description', '" . $get_permalink . "')";
    // ON DUPLICATE KEY UPDATE title = '$post_title', description = '$description', permalink = '" . $get_permalink . "'";
    $result =  $bdd->query($query);
    if (isset($result) && $result) {
        echo "Ajout indexation  meta title et description réussi <br>";
    } else {
        echo "Erreur ajout indexation meta title et description <br>";
    }
}


function insert_Wp_postmeta__thumbnail_id($bdd, $id,$responseImage)
{
    // Insérer ou mettre à jour le SEO Description dans la table wp_postmeta
    $id_image = $responseImage['id_image'];
    $query = "INSERT INTO wp_postmeta (post_id, meta_key, meta_value)
    VALUES ('$id', '_thumbnail_id', $id_image)"; 
     $result =  $bdd->query($query);
}
 